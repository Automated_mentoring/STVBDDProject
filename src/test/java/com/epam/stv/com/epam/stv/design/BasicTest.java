package com.epam.stv.com.epam.stv.design;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;


/**
 * Created by Tatiana_Sauchanka on 2/17/2017.
 */
public class BasicTest {

    protected WebDriver driver;

    @BeforeClass(description = "Start browser")
    public void setUp() {
        new WiggleMainPage(driver).getDriver();
        driver = new ChromeDriver();
        driver.get(START_URL);
        WebElement expectedElement = (new WebDriverWait(driver, 5)).
                until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver webDriver) {
                        return webDriver.findElement(WIGGLE_ICON_LOCATOR);

                    }
                });
    }

    @BeforeClass(dependsOnMethods = "setUp", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterClass
    public void afterClass() throws Exception {
        driver.quit();
    }


}
