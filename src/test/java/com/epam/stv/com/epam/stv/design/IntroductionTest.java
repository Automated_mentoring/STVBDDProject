package com.epam.stv.com.epam.stv.design;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Tatiana_Sauchanka on 2/19/2017.
 */
public class IntroductionTest extends BasicTest {

    @Test
    public void clickOnWiggleIcon() {
        new WiggleSignInPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertRegisterLink() {
        boolean b = new WiggleMainPage(driver).isRegisterLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertSignLink() {
        boolean b = new WiggleMainPage(driver).isSignLinkDisplayed();
        Assert.assertEquals(b, true);
    }

//    Next feature is not stable yet for external Wiggle site
    @Test (dependsOnMethods = {"assertSignLink"})
    public void clickOnSignLink() {
        new WiggleMainPage(driver).clickOnSignLink();
    }

    @Test (dependsOnMethods = {"clickOnSignLink"})
    public void assertEmailAddressFieldIsVisible() {
       boolean b = new WiggleSignInPage(driver).isEmailAddressFieldDisplayed();
       Assert.assertEquals(b, true);
    }

}
