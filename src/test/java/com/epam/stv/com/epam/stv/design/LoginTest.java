package com.epam.stv.com.epam.stv.design;

import com.epam.stv.loginpage.LoginPageValidater;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.stv.locators.LocProjectLocators.*;

/**
 * Created by Tatiana_Sauchanka on 2/17/2017.
 */
public class LoginTest extends BasicTest {

    @Test
    public void assertWiggleIconIsDisplayed() {
        boolean b = new WiggleSignInPage(driver).isWiggleIconDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"inputPassword"}, description = "Make password chars visible")
    public void clickOnShowPassword(){
        new WiggleSignInPage(driver).clickOnShowPassword();
    }

    @Test
    public void inputEmailAddress() {
        new WiggleSignInPage(driver).inputEmailAddress(USER_EMAIL);
    }

    @Test(dependsOnMethods = {"inputEmailAddress"})
    public void inputPassword() {
        new WiggleSignInPage(driver).inputPassword(USER_PASSWORD);
    }

    @Test
    public void assertLoginButtonIsDisplayed() {
        boolean b = new WiggleSignInPage(driver).isLoginButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertLoginButtonIsDisplayed"})
    public void assertLoginButtonLable() {
        String loginLable = new WiggleSignInPage(driver).lableText();
        LoginPageValidater loginPageValidater = new LoginPageValidater();
        boolean b = loginPageValidater.registrExpValidate(loginLable);
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertLoginButtonLable"})
    public void clickLoginButton() {
        new WiggleSignInPage(driver).clickLoginButton();
    }

    @Test(dependsOnMethods = {"clickLoginButton"})
    public void assertWarningAppears() {
        boolean b = new WiggleSignInPage(driver).isWarningMessageDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertWarningAppears"})
    public void assertWarningMessageText() {
        String warningText = new WiggleSignInPage(driver).warningMessageText();
        Assert.assertEquals(warningText, WARNING_TEXT);
    }


}
