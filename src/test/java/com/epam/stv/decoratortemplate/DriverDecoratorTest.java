package com.epam.stv.decoratortemplate;

import com.epam.stv.com.epam.stv.design.WiggleSignInPage;
import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleRegisterFactoryPage;
import com.epam.stv.singlenton.WebDriverSingleton;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.*;

/**
 * Created by Tatiana_Sauchanka on 4/13/2017.
 */

public class DriverDecoratorTest {

    private WebDriver driver;

    @BeforeClass(description = "Start browser")
    public void setUp() {
        String exePath = "C:\\Chromedriver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setJavascriptEnabled(true);
        driver = new ChromeDriver(capabilities);
        driver = new DriverDecorator(driver);
        driver.get(START_URL);
        (new WebDriverWait(driver, 5)).
                until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver webDriver) {
                        return  webDriver.findElement(WIGGLE_ICON_LOCATOR);

                    }
                });
    }

    @BeforeClass(dependsOnMethods = "setUp", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }


    @Test
    public void clickOnWiggleButton(){
        WebElement wiggleButton = driver.findElement(WIGGLE_ICON_LOCATOR);
        wiggleButton.click();
    }

    @Test (dependsOnMethods = "clickOnWiggleButton",description = "Inspect Main page created")
    public void assertMainPage(){
        String registerURL = new WiggleMainFactoryPage(driver).assertCurrentURL();
        Assert.assertEquals(registerURL,MAIN_PAGE);
    }

    @AfterClass
    public void afterClass() throws Exception {
        driver.quit();
    }


}
