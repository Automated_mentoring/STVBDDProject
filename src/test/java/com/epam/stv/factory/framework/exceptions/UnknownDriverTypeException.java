package com.epam.stv.factory.framework.exceptions;

/**
 * Created by Tatiana_Sauchanka on 4/17/2017.
 */
public class UnknownDriverTypeException extends RuntimeException {
    private static final long serialVersionUID = -4330888418140567546L;

    public UnknownDriverTypeException(String msg) {
        super(msg);
    }
}
