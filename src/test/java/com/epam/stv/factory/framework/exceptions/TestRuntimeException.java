package com.epam.stv.factory.framework.exceptions;

/**
 * Created by Tatiana_Sauchanka on 4/17/2017.
 */
public class TestRuntimeException extends RuntimeException{
    private static final long serialVersionUID = 8441781398400036146L;

    public TestRuntimeException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TestRuntimeException(String arg0) {
        super(arg0);
    }

    public TestRuntimeException(Throwable arg0) {
        super(arg0);
    }
}
