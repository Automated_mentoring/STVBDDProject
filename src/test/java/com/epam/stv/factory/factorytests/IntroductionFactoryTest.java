package com.epam.stv.factory.factorytests;


import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleRegisterFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.stv.locators.LocProjectLocators.REGISTER_URL;


/**
 * Created by Tatiana_Sauchanka on 2/19/2017.
 */
public class IntroductionFactoryTest extends BasicFactoryTest {
    @Test
    public void clickOnWiggleIcon() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"clickOnWiggleIconAgain"})
    public void assertRegisterLink() {
        boolean b = new WiggleMainFactoryPage(driver).isRegisterLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertSignLink() {
        boolean b = new WiggleMainFactoryPage(driver).isSignLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"assertSignLink"})
    public void pressTabButton()  {
        new WiggleMainFactoryPage(driver).moveToOverCycleTab();

    }

    @Test (dependsOnMethods = {"pressTabButton"})
    public void assertCycleSubmenu() {
        boolean b = new WiggleMainFactoryPage(driver).isCycleSubmenuDisplayed();
        Assert.assertEquals(b, true);
    }

    //    Next feature is not stable yet for external Wiggle site
    @Test (dependsOnMethods = {"assertCycleSubmenu"})
    public void clickOnSignLink() {
        new WiggleMainFactoryPage(driver).clickOnSignLink();
    }

    @Test (dependsOnMethods = {"clickOnSignLink"})
    public void assertEmailAddressFieldIsVisible() {
            boolean b = new WiggleSignInFactoryPage(driver).isEmailAddressFieldDisplayed();
            Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"assertEmailAddressFieldIsVisible"})
    public void clickOnWiggleIconAgain() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"assertRegisterLink"})
    public void clickOnRegisterLink(){
        new WiggleMainFactoryPage(driver).clickOnRegisterLink();
    }

    @Test (dependsOnMethods = {"clickOnRegisterLink"})
    public void inspectRegisterPage(){
        String registerURL = new WiggleRegisterFactoryPage(driver).assertCurrentURL();
        Assert.assertEquals(registerURL,REGISTER_URL);
    }

    @Test(dependsOnMethods = {"inspectRegisterPage"})
    public void inputPassword() {
        new WiggleRegisterFactoryPage(driver).inputPassword();
    }

}
