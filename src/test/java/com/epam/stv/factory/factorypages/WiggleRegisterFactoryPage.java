package com.epam.stv.factory.factorypages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.epam.stv.factory.framework.utils.RandomPassword;

/**
 * Created by Tatiana_Sauchanka on 4/9/2017.
 */
public class WiggleRegisterFactoryPage extends FactoryPage {

    @FindBy(xpath = "//input[@name='RegisterModel.Password']")
    private WebElement passwordRegisterField;

    public WiggleRegisterFactoryPage(WebDriver driver){
        super(driver);
    }

    public WiggleRegisterFactoryPage inputPassword() {
        RandomPassword rp = new RandomPassword();
        passwordRegisterField.isDisplayed();
        passwordRegisterField.sendKeys(rp.randomString(7));
        return this;
    }


}
