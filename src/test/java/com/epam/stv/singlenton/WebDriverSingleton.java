package com.epam.stv.singlenton;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class WebDriverSingleton {

    private static WebDriver driver;

    private WebDriverSingleton() {
    }

    public static WebDriver getWebDriverInstance() {
        if (driver == null) {
            String exePath = "C:\\Chromedriver\\chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", exePath);
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setJavascriptEnabled(true);
            driver = new ChromeDriver(capabilities);
        }
        return driver;
    }


}
