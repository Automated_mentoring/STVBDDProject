package com.epam.stv.bddframework.framework.steps.wiggle.wiggle;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

/**
 * Created by Tatiana_Sauchanka on 4/15/2017.
 */
//@CucumberOptions(plugin = "json:target/cucumber-report.json")
@CucumberOptions (features = "src/test/java/com/epam/stv/bddframework/features/wiggle/Main page.feature")
public class RunnerBDDTest extends AbstractTestNGCucumberTests {

}
