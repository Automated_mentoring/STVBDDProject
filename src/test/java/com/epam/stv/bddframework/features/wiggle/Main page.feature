Feature: Main page options

  Scenario: 01. The user is able to redirect into the Main page from the Login page and inspect the Main page options
    Given Login page downloaded by default
    When The user presses Wiggle button from login page
    Then Main page is opened
    And Basket button is displayed on the Main page
    And Register link is displayed on the Main page
    And Sign link is displayed on the main page

  Scenario: 02. The user is able to press Basket button from the Main page
    Given Basket button is displayed on the Main page
    When The user presses the Basket button from the main page
    Then Basket page opens

  Scenario: 03. The user redirects into Register from
    When The user presses Wiggle logo from the Main page
    Then Main page is restored
    And The user clicks on Register link from the Main page
    And The Register page opens






