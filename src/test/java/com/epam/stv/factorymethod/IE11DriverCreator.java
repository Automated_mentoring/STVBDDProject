package com.epam.stv.factorymethod;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class IE11DriverCreator extends WebDriverCreator {
    @Override
    public WebDriver driverFactoryMethod() {
        String exePath = "C:\\IEDriverServer_Win32_3.0.0\\IEDriverServer.exe";
        System.setProperty("webdriver.ie.driver", exePath);
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        driver = new InternetExplorerDriver(capabilities);
        capabilities.setPlatform(Platform.WINDOWS);
        return driver;

        // initialize WebDriver for Internet Explorer Driver. Please mind
        // webdriver, iedriverserver version and ie browser versions.
        // works for webdriver v3.0.0beta3, iedriverserver v2.53.1 32 bit, ie
        // browser v11
        // preferable to use iedriverserver v2.53.1 32 bit even for iexplorer 64
        // bit due to stability issues.
        //// System.setProperty("webdriver.ie.driver",
        //// "d:\\_webdriver\\iedriver\\IEDriverServer.exe");
        // DesiredCapabilities capabilities =
        //// DesiredCapabilities.internetExplorer();
        // driver = new InternetExplorerDriver(capabilities);
    }
}
