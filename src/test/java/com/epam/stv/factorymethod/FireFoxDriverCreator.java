package com.epam.stv.factorymethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class FireFoxDriverCreator extends WebDriverCreator{
    @Override
    public WebDriver driverFactoryMethod() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setJavascriptEnabled(true);
        driver = new FirefoxDriver(capabilities);
        return driver;
    }
}
