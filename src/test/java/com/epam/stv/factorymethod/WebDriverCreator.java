package com.epam.stv.factorymethod;

import org.openqa.selenium.WebDriver;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public abstract class WebDriverCreator {

    protected WebDriver driver;

    public abstract WebDriver driverFactoryMethod();



}
